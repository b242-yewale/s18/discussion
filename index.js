function printInput(){
	let nickName = prompt("Enter your nickname: ");
	console.log("Hi, "+nickName);
}

// printInput();

function printName(name){
	console.log("My name is	"+name);
}

// printName("Mayuri");

// [Section] Variable passed as an argument
let sampleVariable = "Onkar";
printName(sampleVariable);

// Create two different functions that will display a person's age and location using function parameters and arguments

function printAge(age){
	console.log("My age is "+age);
}

function printLocation(location){
	console.log("My location is "+location);
}

/*let inputAge = 29;
let inputLocation = "Mumbai";*/

printAge(29);
printLocation("Mumbai");

function checkDivisibilityBy8(num){
	let reminder = num % 8;
	console.log("The reminder of "+ num +" divided by 8 is "+reminder);
	let isDivisibleBy8 = reminder === 0;
	console.log("Is "+ num +" divisible by 8?");
	console.log(isDivisibleBy8);
}

checkDivisibilityBy8(64);
checkDivisibilityBy8(57);

// [Section] Functions as Arguments

function agrumentFunction(){
	console.log("This function was passed as an argument before the message was printed.");
}

function invokeFunction(invokFunction){
	invokFunction();
}

invokeFunction(agrumentFunction);
console.log(agrumentFunction);  //Displays more info about the argumentFunction

// [Section] Using multiple parameters

// Using multiple parameters
function createFullName(firstName, middleName, lastName){
	console.log(firstName + " " + middleName + " " + lastName);
}

createFullName("Onkar", "Prakash", "Yewale");
createFullName("Aarush", "Onkar", "Yewale");
createFullName("Mayuri", " ", "Yewale");
createFullName("Full", "Name");
createFullName("Onkar", "Aarush", "Mayuri", "Yewale");

function printFullName(middleName, lastName, firstName){
	console.log(firstName + " " + middleName + " " + lastName);
}

printFullName("Onkar", "Prakash", "Yewale");

// Return Statement
// Allows to output vlaue from a function to be passed to the line/block of code that invoced the function.

function returnFullName(firstName, middleName, lastName){
	return firstName + " " + middleName + " " + lastName;
	console.log("This message will not be printed."); 
}

console.log(returnFullName("Onkar", "Prakash", "Yewale"));
let completeName = returnFullName("Onkar", "Prakash", "Yewale");
console.log(completeName);

function returnAddress(city, country){
	let fullAddress = city + ", " + country;
	return fullAddress;
}

let myAddress = returnAddress("Mumbai", "India");
console.log(myAddress);

function printPlayerInfo(username, level, job){
	console.log("Username: "+username);
	console.log("Level: "+level);
	console.log("Job: "+job);
}

let user1 = printPlayerInfo("Horse", 2, "2.5 Steps");
console.log(user1);